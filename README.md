https://www.atlassian.com/git/tutorials/git-subtree


* Next steps:
* add subrepo changes, then pull them within main repo
    * probably, these changes need to be squashed again so that they can be added to the main repo
* then add another change from main repo and push to subrepo
* let subrepo and main repo/subfolder diverge and merge changes
    * within main repo
    * within subrepo

```bash
10627  mkdir subtree_test_main_repo
10628  mkdir subtree_test_subrepo
10629  cd subtree_test_main_repo
10630  git init
10631  touch file1.py
10632  touch file2.py
10633  git add file1.py
10634  git commit -m "Add file1.py"
10635  git add file2.py
10636  git commit -m "Add file2.py"
10637  ll
10638  cd ..
10639  ll
10640  cd subtree_test_subrepo
10641  ll
10642  git init
10643  touch file1.py
10644  git add file1.py
10645  git commit -m "Add file1.py"
10646  pwd
10647  git remote add origin git@gitlab.com:theRealSuperMario/subtree_test_subrepo.git
10648  cd ..
10649  ll
10650  cd subtree_test_main_repo
10651  ll
10652  git remote add origin git@gitlab.com:theRealSuperMario/subtree_test_main_repo.git
10653  git push origin master
10654  cd ..
10655  cd subtree_test_subrepo
10656  git push origin master
10657  ll
10658  cd ..
10659  ll
10660  cd subtree_test_main_repo
10661  ll
10662  git subtree add --prefix subrepo git@gitlab.com:theRealSuperMario/subtree_test_subrepo.git
10663  git remote add -f subrepo git@gitlab.com:theRealSuperMario/subtree_test_subrepo.git
10664  git status
10665  ll
10666  git subtree add --prefix subrepo_folder subrepo master --squash
10667  ll
10668  git status
10669  cd subrepo_folder
10670  git status
10671  cd ..
10672  code .
10673  cd subrepo_folder
10674  ll
10675  touch file2.py
10676  cd ..
10677  git status
10678  git add subrepo_folder/file2.py
10679  git commit -m "Add subrepo_folder/file2.py"
10680  git subtree push --prefix subrepo_folder subrepo master git push
10681  git subtree push --prefix subrepo_folder subrepo master
10682* cd Projekte/gitlab_projects/subtree_test_subrepo
10683* git pull origin master
10684  git log
10685  code .
```


![Final commit history](assets/final_commit_history.png)